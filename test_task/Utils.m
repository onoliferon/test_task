//
//  Utils.m
//  test_task
//
//  Created by Olifer Nikita on 16.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (CGFloat) getHeightForText: (NSString*) text width: (CGFloat) width font: (UIFont*) font
{
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName: font
     }];
    CGRect rect = [attributedText boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX)
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    return rect.size.height;
}

@end
