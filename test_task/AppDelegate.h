//
//  AppDelegate.h
//  test_task
//
//  Created by Olifer Nikita on 15.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
