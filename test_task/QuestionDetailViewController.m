//
//  QuestionDetailViewController.m
//  test_task
//
//  Created by Olifer Nikita on 16.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "QuestionDetailViewController.h"
#import "Utils.h"

@interface QuestionDetailViewController ()

@end

@implementation QuestionDetailViewController

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}

#define MARGIN 5.0f

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGFloat screenWidth = CGRectGetWidth(self.view.bounds);
    CGFloat textWidth = screenWidth - 2 * MARGIN;
    
    // Setup scroll view.
    UIScrollView *scrollview = [[UIScrollView alloc] init];
    
    scrollview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    scrollview.translatesAutoresizingMaskIntoConstraints = NO;
    scrollview.showsVerticalScrollIndicator=YES;
    scrollview.scrollEnabled=YES;
    scrollview.userInteractionEnabled=YES;
    scrollview.contentSize = CGSizeMake(screenWidth, CGFLOAT_MAX);

    [self.view addSubview:scrollview];
    
    
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[scrollview]|"
                                             options:0
                                             metrics:nil
                                             views:NSDictionaryOfVariableBindings(scrollview)]];
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scrollview]|"
                                             options:0
                                             metrics:nil
                                             views:NSDictionaryOfVariableBindings(scrollview)]];
    
    // Setup question label.
    UILabel* questionLabel = [[UILabel alloc] init];
    questionLabel.text = _question;
    questionLabel.numberOfLines = 0;
    questionLabel.translatesAutoresizingMaskIntoConstraints = NO;

    
    [scrollview addSubview:questionLabel];
    [scrollview addConstraints:[NSLayoutConstraint
                              constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[questionLabel(==%f)]", MARGIN, textWidth]
                              options:NSLayoutFormatDirectionLeadingToTrailing
                              metrics:nil
                              views:NSDictionaryOfVariableBindings(questionLabel)]];

    // Setup answer label.
    UILabel* answerLabel = [[UILabel alloc] init];
    answerLabel.text = _answer;
    answerLabel.numberOfLines = 0;
    answerLabel.translatesAutoresizingMaskIntoConstraints = NO;
    answerLabel.textColor = UIColorFromRGB(0x00B88A);
    
    
    [scrollview addSubview:answerLabel];
    
    [scrollview addConstraints:[NSLayoutConstraint
                                constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[answerLabel(==%f)]", MARGIN, textWidth]
                                options:NSLayoutFormatDirectionLeadingToTrailing
                                metrics:nil
                                views:NSDictionaryOfVariableBindings(answerLabel)]];
    
    
    // Calculate heights and setup constraints.
    CGFloat questionHeight = [Utils getHeightForText:_question width:textWidth font:questionLabel.font] + MARGIN;
    CGFloat answerHeight = [Utils getHeightForText:_answer width:textWidth font:answerLabel.font] + MARGIN;
    
    
    [scrollview addConstraints:[NSLayoutConstraint
                                constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[questionLabel(==%f)]-[answerLabel(==%f)]|", questionHeight, answerHeight]
                                options:NSLayoutFormatDirectionLeadingToTrailing
                                metrics:nil
                                views:NSDictionaryOfVariableBindings(questionLabel, answerLabel)]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
