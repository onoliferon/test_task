//
//  TaskTableViewController.m
//  test_task
//
//  Created by Olifer Nikita on 16.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import "TaskTableViewController.h"
#import "QuestionDetailViewController.h"
#import "Utils.h"

#define QUESTION_DETAIL_SEGUE @"question_detail_segue"

@interface TaskTableViewController ()

/**
 Two dimensional array of questions by groups.
 */
@property (strong, nonatomic) NSArray* questions;

/**
 Question groups.
 */
@property (strong, nonatomic) NSArray* questionGroups;

/**
 Two dimensional array of answers by groups.
 */
@property (strong, nonatomic) NSArray* answers;

@end

@implementation TaskTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.translucent = NO;
    
    _questions = [NSArray arrayWithObjects:
                     [NSArray arrayWithObjects:
                          @"В чем отличие weak, strong, assign свойств классов",
                          @"Как сделать приватным и публичным свойство или метод в obj-c",
                          @"Для чего нужны протоколы, что такое @optional и @required свойства и методы",
                          @"Что делает блок ^(void){} ",
                          nil],
                     [NSArray arrayWithObjects:
                          @"Опиши как минимум 3 способа дизайна приложения",
                          @"Каким способом из указанных ты пользуешься постоянно?",
                          @"Что такое ограничения и как с ними можно работать?",
                          @"Знаком ли ты с языком визуального форматирования ?",
                          @"Что значит выражение H:|-100-[_button]-100-| и V:[_button]-(>=100)-|",
                          @"Для чего нужны segue и как их используют ?",
                          nil],
                     nil];
    _questionGroups = [NSArray arrayWithObjects:@"Вопросы по objective-c", @"Вопросы по Cocoa", nil];
    _answers = [NSArray arrayWithObjects:
                    [NSArray arrayWithObjects:
                            @"strong - объект будет существовать в памяти всегда, пока на него есть хоть одна такая ссылка;"\
                            @"weak - такая ссылка не будет удерживать объект, если на него не останется strong-ссылок (в этом случае память может освободиться);"\
                            @"assign - аналогично weak, только, когда объект удаляется, то ссылка не выставляется автоматически в nil (применяется для простых типов).",
                            @"Публичные методы и свойства объявляются в заголовочном файле. Приватные свойства и методы объявляются только в файлах реализации.",
                            @"Протоколы нужны, чтобы обязать класс реализовать определённые методы. Часто используются, например, чтобы один объект обрабатывал события"\
                            @"или поставлял данные для другого. Для этого в класс одного объекта добавляется протокол, обязывающий реализовать определённые методы,"\
                            @"нужные другому объекту.\n"\
                            @"optional - реализация метода является не обязательной;\n"\
                            @"required - обязательной.",
                            @"Ничего. Это блок, не возвращающий значения и не выполняющий какого-либо кода.",
                        nil],
                    [NSArray arrayWithObjects:
                            @"Если имеется в виду способы задания размеров и положения элементов:\n"\
                            @"1) Можно задавать все параметры вручную через изменение свойства frame, center, bounds, authoresizingMask объектов (это как бы устаревший способ).\n"\
                            @"2) Можно в визуальном редакторе создавать constraints.\n"\
                            @"3) Можно эти же constraints прописывать руками в коде.",
                            @"Я задаю constraints из визуального редактора. Изредка приходится создавать ссылки на constraint для изменения значения динамически.",
                            @"Ограничения (constraint) - это такие объекты, которые ограничивают положение или размер объектов интерфейса. Можно задавать из визуального редактора, или создавать вручную. Можно динамически менять свойство constant.",
                            @"Знаком, но применять ранее его не доводилось. Обходился заданием ограничений в визуальном редакторе.",
                            @"H:|-100-[_button]-100-| - кнопка должна быть на расстоянии 100 от левой и правой границ superview;\n"\
                            @"V:[_button]-(>=100)-| - кнопка должна быть на расстоянии больше или равном 100 от нижней границы superview.",
                            @"segue нужны для задания переходов между ViewController'ами. Они позволяют сделать переход проще и наглядно показываются на storyboard. В методе prepareForSegue можно передавать данные вызываемому контроллеру.",
                        nil],
                    nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _questionGroups.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_questions objectAtIndex:section] count];
}


#define MAIN_LABEL_TAG 1
#define INDEX_LABEL_TAG 2

#define MARGIN 10.0f
#define INDEX_LABEL_WIDTH 25.0f
#define MIN_CELL_HEIGHT 40.0f

#define MAIN_LABEL_FONT_SIZE 14
#define INDEX_LABE_FONT_SIZE 22



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* identifier = @"task_cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    UILabel* mainLabel;
    UILabel* indexLabel;
    
    if(cell == nil) {
        // Create not existing cell.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        
        mainLabel = [[UILabel alloc] init];
        mainLabel.numberOfLines = 0;
        mainLabel.translatesAutoresizingMaskIntoConstraints = NO;
        mainLabel.font = [UIFont systemFontOfSize:MAIN_LABEL_FONT_SIZE];
        mainLabel.tag = MAIN_LABEL_TAG;
        
        indexLabel = [[UILabel alloc] init];
        indexLabel.translatesAutoresizingMaskIntoConstraints = NO;
        indexLabel.font = [UIFont systemFontOfSize:INDEX_LABE_FONT_SIZE];
        indexLabel.tag = INDEX_LABEL_TAG;
        

        [cell.contentView addSubview:mainLabel];
        [cell.contentView addSubview:indexLabel];
        

        // Setup constraints.
        [cell.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[mainLabel]|"]
                                        options:NSLayoutFormatDirectionLeadingToTrailing
                                        metrics:nil
                                        views:NSDictionaryOfVariableBindings(mainLabel)]];
        
        [cell.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-%f-[indexLabel(==%f)]-%f-[mainLabel]-%f-|", MARGIN, INDEX_LABEL_WIDTH, MARGIN, MARGIN]
                                          options:NSLayoutFormatDirectionLeadingToTrailing
                                          metrics:nil
                                          views:NSDictionaryOfVariableBindings(mainLabel, indexLabel)]];
        
        [cell.contentView addConstraints:[NSLayoutConstraint
                                          constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[indexLabel]|"]
                                          options:NSLayoutFormatDirectionLeadingToTrailing
                                          metrics:nil
                                          views:NSDictionaryOfVariableBindings(indexLabel)]];
        
    } else {
        // Cell already exists.
        mainLabel = (UILabel*) [cell.contentView viewWithTag:MAIN_LABEL_TAG];
        indexLabel = (UILabel*) [cell.contentView viewWithTag:INDEX_LABEL_TAG];
    }
    
    // Setup text and index label.
    mainLabel.text = [[_questions objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    indexLabel.text = [NSString stringWithFormat:@"%d", indexPath.row + 1];
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [_questionGroups objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSString *text = [[_questions objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    CGFloat height = [Utils getHeightForText:text width:CGRectGetWidth(self.view.bounds) - INDEX_LABEL_WIDTH - 3 * MARGIN font:[UIFont systemFontOfSize:MAIN_LABEL_FONT_SIZE]];
    height += 2 * MARGIN;
    
    if(height > MIN_CELL_HEIGHT) {
        return height;
    }
    
    return MIN_CELL_HEIGHT;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:QUESTION_DETAIL_SEGUE sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:QUESTION_DETAIL_SEGUE]) {
        // Go to answer.
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        QuestionDetailViewController* controller = segue.destinationViewController;
        
        controller.question = [[_questions objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        controller.answer = [[_answers objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
}


@end
