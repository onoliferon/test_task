//
//  main.m
//  test_task
//
//  Created by Olifer Nikita on 15.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
