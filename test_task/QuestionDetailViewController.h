//
//  QuestionDetailViewController.h
//  test_task
//
//  Created by Olifer Nikita on 16.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionDetailViewController : UIViewController

@property (weak, nonatomic) NSString* question;
@property (weak, nonatomic) NSString* answer;

@end
