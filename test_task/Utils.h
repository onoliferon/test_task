//
//  Utils.h
//  test_task
//
//  Created by Olifer Nikita on 16.07.14.
//  Copyright (c) 2014 fer-comp. All rights reserved.
//

#import <Foundation/Foundation.h>

// UIColor from hex value.
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


// Class with additional common methods.
@interface Utils : NSObject

/**
 Calculates size of multiline text.
 */
+ (CGFloat) getHeightForText: (NSString*) text width: (CGFloat) width font: (UIFont*) font;

@end
